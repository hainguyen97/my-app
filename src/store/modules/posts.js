import axios from 'axios';

// initial state
const state = {
    postAll: [],
    deletePost: Boolean,
    edit: {
        id: null,
        title: null,
        body: null
    },
    editForm: -1,
    postIDCache: null,
    dialog: false
}

// getters
const getters = {}

// actions
const actions = {
    async getAllPosts({ commit }) {
        let data = await axios.get('http://localhost:3000/posts?_sort=id&_order=desc')
        commit('setAllPosts', data.data);
    },
    async deletePost({ commit }, postID) {
        let data = await axios({
            method: 'delete',
            url: `http://localhost:3000/posts/${postID}`,
        })
        if(data.status == 200){
            commit('deletePost');
        }
    },
    async addNewPost({ commit }, newPost) {
        let {title, body} = newPost
        let data = await axios({
            method: 'post',
            url: `http://localhost:3000/posts`,
            data: {title, body}
        })
        commit('addPost', data.data);
    },
    async updatePost({commit}, newPost){
        console.log(newPost);
        let {id, title, body} = newPost
        let data = await axios({
            method: 'put',
            url: `http://localhost:3000/posts/${id}`,
            data: {title, body}
        })
        commit('updatePost', data.data);
    }
}

// mutations
const mutations = {
    setAllPosts(state, data) {
        state.postAll = data
    },
    deletePost(state){
        state.deletePost = true
    },
    addPost(state, data){
        state.postIDCache = data.id
    },
    updatePost(state, data){
        state.postIDCache = data.id
    },
    triggerDialog(state){
        state.dialog = !state.dialog
    },
    setFormEdit(state, data){
        state.editForm = data
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}