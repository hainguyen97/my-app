import router from '../router'

export default {
    redirect: (error) =>{
        if(error.response)
            if(error.response.status == 404){
                router.push({name: 'notFound'})
            }
    }
}
