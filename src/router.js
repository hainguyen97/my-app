import VueRouter from "vue-router"
import ListPost from "./components/ListPost"
import PostComp from "./components/PostComp"
import PostDetail from './components/PostDetail'
import Dashboard from './components/Dashboard';
import NotFound from './components/404';
import Login from './components/Login';
import Home from './components/Home'

import Vue from "vue"

Vue.use(VueRouter);

const routes = [
    {
        path: '/', component: Home, children: [
            { path: '/dashboard', component: Dashboard, name: 'dashboard' },
            {
                path: '/post', component: PostComp,
                children: [
                    {
                        path: '',
                        component: ListPost,
                        name: 'listPost'
                    },
                    {
                        path: ':id',
                        component: PostDetail,
                        name: 'postDetail'
                    }

                ]
            },
        ]
    },

    { path: '/login', component: Login, name: 'login' },
    { path: '*', redirect: '/not-found' },
    { path: '/not-found', component: NotFound, name: 'notFound' }

]

const router =  new VueRouter({
    mode: "history",
    routes // short for `routes: routes`
})

router.beforeEach((to, from, next) => {
    if(to.name !== 'login' && !localStorage.getItem('token')){
        return next({name: 'login'})
    }
    return next()
  })

export default router
